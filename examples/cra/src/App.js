import React from 'react';
import './App.css';
import { Nav } from '@bry/react-ui-components';

function App() {
  return (
    <div>
      <Nav />
    </div>
  );
}

export default App;
