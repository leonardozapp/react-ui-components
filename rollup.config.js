import babel from 'rollup-plugin-babel'
import resolve from '@rollup/plugin-node-resolve'
import scss from 'rollup-plugin-scss'
import autoprefixer from 'autoprefixer'
import postcss from 'postcss'
import pkg from './package.json'

export default {
  input: 'src/index.js',
  external: ['react', 'prop-types'],
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true
    },
    {
      file: pkg.module,
      format: 'esm',
      sourcemap: true
    }
  ],
  plugins: [
    resolve(),
    babel({ exclude: 'node_modules/**' }),
    scss({
      processor: css =>
        postcss({ modules: true, plugins: [autoprefixer] })
          .process(css)
          .then(result => result.css)
    })
  ]
}
