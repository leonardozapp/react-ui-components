import React from 'react'
import PropTypes from 'prop-types'
import './Button.scss'

const Button = ({
  children,
  type = 'button',
  href,
  onClick,
  variant,
  size,
  disabled = false,
  className,
  linkComponent
}) => {
  const VARIANT = VARIANTS.includes(variant) ? variant : VARIANTS[0]
  const SIZE = SIZES.includes(size) ? size : SIZES[0]

  const Link = linkComponent || 'a'

  return (
    <>
      {href ? (
        <Link
          className={`Button Button__${VARIANT} Button__${SIZE} ${className ||
            ''}`}
          type={type}
          href={href}
          onClick={onClick}
          disabled={disabled}
          variant={variant}
          size={size}
        >
          {children}
        </Link>
      ) : (
        <button
          className={`Button Button__${VARIANT} Button__${SIZE} ${className ||
            ''}`}
          type={type}
          onClick={onClick}
          disabled={disabled}
          variant={variant}
          size={size}
        >
          {children}
        </button>
      )}
    </>
  )
}

const VARIANTS = ['primary', 'secondary', 'primary-inverted']
const SIZES = ['regular', 'large', 'small']

Button.propTypes = {
  children: PropTypes.node.isRequired,
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  href: PropTypes.string,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(VARIANTS),
  size: PropTypes.oneOf(SIZES),
  disabled: PropTypes.bool,
  className: PropTypes.string,
  linkComponent: PropTypes.elementType
}

Button.defaultProps = {
  type: 'button',
  variant: VARIANTS[0],
  size: SIZES[0],
  disabled: false
}

export default Button
