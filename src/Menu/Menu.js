import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import './Menu.scss'
import { IconChevronDown } from '../Icons'

const Menu = props => {
  const [selectedIndex, setSelectedIndex] = useState('')
  const GLOBAL_WINDOW =
    (typeof self === 'object' && self.self === self && self) ||
    (typeof global === 'object' && global.global === global && global) ||
    this
  const Link = props.linkComponent || 'a'

  useEffect(() => {
    if (typeof window === 'undefined') {
      GLOBAL_WINDOW.addEventListener('click', handleMenuClick)
    } else {
      window.addEventListener('click', handleMenuClick)
    }
    return () => {
      if (typeof window === 'undefined') {
        GLOBAL_WINDOW.removeEventListener('click', handleMenuClick)
      } else {
        window.removeEventListener('click', handleMenuClick)
      }
    }
  })

  function handleMenuClick (e) {
    if (selectedIndex === 0 || selectedIndex === 1) {
      setSelectedIndex('')
    }

    if (Link === 'a') {
      if (e.target.classList.value.includes('no-sub')) {
        e.preventDefault()
        props.onHandleMobile(false)
        setTimeout(() => {
          window.location = e.target.href
        }, 1000)
      }
    }
  }

  const generateSubMenu = subMenuItems => (
    <>
      {subMenuItems.map((subMenuItem, index) => {
        return (
          <div key={index} className="Menu__sub-item-wrapper">
            {subMenuItem.title && (
              <h2 className="Menu__sub-title">{subMenuItem.title}</h2>
            )}
            <ul className="Menu__sub-items">
              {subMenuItem.items.map(item => (
                <li key={item.title} className="Menu__sub-item">
                  <Link
                    href={item.link}
                    title={item.title}
                    className="Menu__sub-title"
                  >
                    {item.icon && (
                      <div className="Menu__sub-icon">{item.icon}</div>
                    )}
                    <div>
                      {item.title}

                      <p className="Menu__sub-dec">{item.desc}</p>
                    </div>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )
      })}
    </>
  )

  const generateMobileSubMenu = subMenuItems => (
    <>
      {subMenuItems.map((subMenuItem, index) => {
        return (
          <div key={index} className="Menu__sub-item-wrapper">
            <ul className="Menu__sub-items">
              {subMenuItem.items.map(item => (
                <li key={item.title} className="Menu__sub-item ">
                  <Link
                    href={item.link}
                    title={item.title}
                    onClick={e => {
                      const t = e.target
                      props.onHandleMobile(false)
                      setTimeout(function () {
                        t.dispatchEvent(e)
                      }, 1000)
                      return false
                    }}
                  >
                    {item.title}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        )
      })}
    </>
  )

  return (
    <ul id="menu" className="Menu">
      {props.menuItems.map(({ title, link, subMenu }, index) => (
        <li className="Menu__item" key={title}>
          <div
            onClick={() => {
              setSelectedIndex(selectedIndex === index ? '' : index)
            }}
          >
            {link ? (
              <Link
                href={link}
                className="Menu__item-title no-sub"
                onClick={() => props.onHandleMobile(false)}
              >
                {title}
              </Link>
            ) : (
              <div className="Menu__item-title">
                <span>{title}</span>
                {subMenu && (
                  <div className="Menu__icon-wrapper">
                    <IconChevronDown
                      className={selectedIndex === index ? 'rotate' : ''}
                    />
                  </div>
                )}
              </div>
            )}
          </div>
          {selectedIndex === index && subMenu && (
            <div className="Menu__sub">
              <div className="Menu__sub-container container">
                {props.isMobile
                  ? generateMobileSubMenu(subMenu)
                  : generateSubMenu(subMenu)}
              </div>
            </div>
          )}
        </li>
      ))}
    </ul>
  )
}

Menu.propTypes = {
  menuItems: PropTypes.array,
  isMobile: PropTypes.bool,
  onMobileOpen: PropTypes.func,
  onHandleMobile: PropTypes.func,
  linkComponent: PropTypes.elementType
}

export default Menu
