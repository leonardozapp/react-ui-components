import React from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  className: PropTypes.string
}

const IconChevronDown = ({ className }) => {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="7.41"
      viewBox="0 0 12 7.41"
    >
      <title>Icone Chevron Down</title>
      <path d="M1.41,0,6,4.58,10.59,0,12,1.41l-6,6-6-6Z" fill="#97a1a6" />
      <path d="M-6-8.59H18v24H-6Z" fill="none" />
    </svg>
  )
}

IconChevronDown.propTypes = propTypes

export default IconChevronDown
