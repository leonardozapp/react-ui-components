import React from 'react'

const IconCertificadoDigital = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="44.51"
      height="51"
      viewBox="0 0 44.51 51"
    >
      <defs>
        <linearGradient
          id="a"
          x1="-1308.35"
          y1="399.04"
          x2="-1307.35"
          y2="399.04"
          gradientTransform="matrix(33.42, 0, 0, -50.64, 43730.71, 20232.54)"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stopColor="#f2994a" />
          <stop offset="1" stopColor="#ee5757" />
        </linearGradient>
        <linearGradient
          id="b"
          x1="-1286.37"
          y1="390.61"
          x2="-1285.37"
          y2="390.61"
          gradientTransform="matrix(19.92, 0, 0, -19.92, 25642, 7799.5)"
          href="#a"
        />
      </defs>
      <title>Icone certificado digital</title>
      <path
        d="M39,16.93a16.71,16.71,0,1,0-27.63,12.6V48.3A2.4,2.4,0,0,0,15.42,50l6.28-6.27a.82.82,0,0,1,1.13,0l6.38,6.38a2.38,2.38,0,0,0,1.68.71,2.54,2.54,0,0,0,.94-.19,2.36,2.36,0,0,0,1.48-2.21v-19A16.65,16.65,0,0,0,39,16.93Zm-31.82,0A15.12,15.12,0,1,1,22.25,32,15.12,15.12,0,0,1,7.14,16.93ZM31.7,48.41a.8.8,0,0,1-.8.8.79.79,0,0,1-.56-.23L24,42.6a2.46,2.46,0,0,0-3.4,0l-6.27,6.27a.81.81,0,0,1-1.37-.56V30.78a16.64,16.64,0,0,0,18.78-.09Z"
        fill="url(#a)"
      />
      <path
        d="M22.25,26.89a10,10,0,1,0-10-10A10,10,0,0,0,22.25,26.89Zm0-18.29a8.33,8.33,0,1,1-8.33,8.33A8.33,8.33,0,0,1,22.25,8.6Z"
        fill="url(#b)"
      />
    </svg>
  )
}

export default IconCertificadoDigital
