import React from 'react'

const IconBryFramework = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      viewBox="0 0 32 32"
    >
      <title>Icone Bry Framework</title>
      <path
        d="M30.47,32H1.53A1.52,1.52,0,0,1,0,30.5v-7A1.52,1.52,0,0,1,1.53,22H30.47A1.52,1.52,0,0,1,32,23.5v7A1.52,1.52,0,0,1,30.47,32ZM1.53,23a.51.51,0,0,0-.53.5v7a.51.51,0,0,0,.53.5H30.47a.51.51,0,0,0,.53-.5v-7a.51.51,0,0,0-.53-.5Zm28.94-2H1.53A1.52,1.52,0,0,1,0,19.5v-7A1.52,1.52,0,0,1,1.53,11H30.47A1.52,1.52,0,0,1,32,12.5v7A1.52,1.52,0,0,1,30.47,21ZM1.53,12a.51.51,0,0,0-.53.5v7a.51.51,0,0,0,.53.5H30.47a.51.51,0,0,0,.53-.5v-7a.51.51,0,0,0-.53-.5Zm28.94-2H1.53A1.52,1.52,0,0,1,0,8.5v-7A1.52,1.52,0,0,1,1.53,0H30.47A1.52,1.52,0,0,1,32,1.5v7A1.52,1.52,0,0,1,30.47,10ZM1.53,1A.51.51,0,0,0,1,1.5v7a.51.51,0,0,0,.53.5H30.47A.51.51,0,0,0,31,8.5v-7a.51.51,0,0,0-.53-.5ZM8,5.5A.5.5,0,0,0,7.5,5h-3a.5.5,0,0,0,0,1h3A.5.5,0,0,0,8,5.5Zm0,11a.5.5,0,0,0-.5-.5h-3a.5.5,0,0,0,0,1h3A.5.5,0,0,0,8,16.5Zm0,11a.5.5,0,0,0-.5-.5h-3a.5.5,0,0,0,0,1h3A.5.5,0,0,0,8,27.5Z"
        fill="#009cda"
      />
    </svg>
  )
}

export default IconBryFramework
