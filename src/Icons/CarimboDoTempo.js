import React from 'react'

const IconCarimboDoTempo = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="49"
      height="49"
      viewBox="0 0 49 49"
    >
      <defs>
        <linearGradient
          id="a"
          y1="24.5"
          x2="49"
          y2="24.5"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stopColor="#ff9a38" />
          <stop offset="1" stopColor="#f55" />
        </linearGradient>
      </defs>
      <title>Icone carimbo do tempo</title>
      <path
        d="M24.5,1.53a23,23,0,1,1-23,23,23,23,0,0,1,23-23M24.5,0A24.5,24.5,0,1,0,49,24.5,24.5,24.5,0,0,0,24.5,0Zm8.21,31.94a.76.76,0,0,0,0-1.08L26,24.18V8.42a.77.77,0,0,0-1.53,0v16.4l7.13,7.12a.76.76,0,0,0,.54.23A.78.78,0,0,0,32.71,31.94Z"
        fill="url(#a)"
      />
    </svg>
  )
}

export default IconCarimboDoTempo
