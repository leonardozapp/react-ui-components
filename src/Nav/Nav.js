import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import Menu from '../Menu'
import LogoBRyTecnologia from '../Logos/BryTecnologia'
import {
  IconAssinaturaDigital,
  IconBryAtributos,
  IconBryFramework,
  IconBryIcp,
  IconBryKms,
  IconBryPdde,
  IconBryPssDeAct,
  IconBryScad,
  IconBrySct,
  IconBrySigner,
  IconCarimboDoTempo,
  IconCertificadoDigital
} from '../Icons'

import './Nav.scss'

const Nav = ({ domain = '', linkComponent, showTesteGratis = true }) => {
  const [isMobileMenuOpen, setisMobileMenuOpen] = useState(false)
  const Link = linkComponent || 'a'

  const menuItems = [
    {
      title: 'Serviços',
      link: '',
      subMenu: [
        {
          items: [
            {
              icon: <IconAssinaturaDigital />,
              title: 'Assinar Documentos Eletrônicos',
              desc:
                'Assine, verifique e colete assinaturas Eletrônicas, Digitais e Híbridas com segurança e validade jurídica.',
              link: `${domain}/assinatura-digital/`
            }
          ]
        },
        {
          items: [
            {
              icon: <IconCertificadoDigital />,
              title: 'Comprar e Hospedar Certificados Digitais',
              desc:
                'Emita ou armazene seu certificado digital e assine documentos eletrônicos de qualquer dispositivo.',
              link: `${domain}/certificados-digitais/`
            }
          ]
        },
        {
          items: [
            {
              icon: <IconCarimboDoTempo />,
              title: 'Integrar Carimbo do Tempo',
              desc:
                'Integre o carimbo do tempo na sua aplicação e garanta a data e a hora de fonte confiável no seu documento.',
              link: `${domain}/carimbo-do-tempo/`
            }
          ]
        }
      ]
    },
    {
      title: 'Produtos',
      link: '',
      subMenu: [
        {
          title: 'Assinar, Coletar e Verificar assinaturas digitais',
          items: [
            {
              icon: <IconBrySigner />,
              title: 'BRy Signer Desktop',
              product: 'Signer Desktop',
              desc:
                'Software de assinatura e verificação de documentos digitais',
              link: 'https://signer.bry.com.br/'
            },
            {
              icon: <IconBryFramework />,
              title: 'BRy Framework',
              product: 'Framework',
              desc: 'Servidor de geração e verificação de assinaturas digitais',
              link: `${domain}/bry-framework/`
            },
            {
              icon: <IconBryScad />,
              title: 'BRy SCAD',
              product: 'SCAD',
              desc: 'Sistema online de coleta de assinaturas digitais',
              link: `${domain}/bry-scad/`
            }
          ]
        },
        {
          title:
            'Hospedar Certificados Digitais ou Tornar-se uma Autoridade Certificadora',
          items: [
            {
              icon: <IconBryKms />,
              title: 'BRy KMS',
              product: 'KMS',
              desc:
                'Solução que torna o certificado acessível de qualquer dispositivo.',
              link: `${domain}/bry-kms/`
            },
            {
              icon: <IconBryAtributos />,
              title: 'BRy Atributos',
              product: 'Atributos',
              desc:
                'Sistema de gerenciamento de Entidades Emissoras de Atributos EEA.',
              link: `${domain}/bry-atributos/`
            },
            {
              icon: <IconBryIcp />,
              title: 'BRy ICP',
              product: 'ICP',
              desc:
                'Plataforma para instalação de Autoridades Certificadoras.',
              link: `${domain}/bry-icp/`
            }
          ]
        },
        {
          title: 'Protocolar ou carimbar digitalmente',
          items: [
            {
              icon: <IconBryPdde />,
              title: 'BRy PDDE',
              product: 'PDDE',
              desc: 'Servidor para geração de protocolos digitais.',
              link: `${domain}/bry-pdde/`
            },
            {
              icon: <IconBryPssDeAct />,
              title: 'BRy Pss de ACT',
              product: 'Pss de ACT',
              desc:
                'Prestação de serviço de suporte para Autoridade de Carimbo do Tempo.',
              link: `${domain}/bry-pss-de-act/`
            },
            {
              icon: <IconBrySct />,
              title: 'BRy SCT',
              product: 'SCT',
              desc: 'Sistema de Carimbo do Tempo.',
              link: `${domain}/bry-sct/`
            }
          ]
        }
      ]
    },
    { title: 'Cases', link: `${domain}/cases/` },
    { title: 'Blog', link: 'https://blog.bry.com.br/' },
    { title: 'Entrar', link: 'https://cloud.bry.com.br/home/restrito/poslogin' }
  ]

  function handleMobileOpen (value) {
    setisMobileMenuOpen(value)
  }
  return (
    <>
      <nav className="Nav">
        <div className="Nav__container container">
          <div className="Nav__logo-wrapper">
            <div className="Nav__logo">
              <Link href={`${domain}/`}>
                <LogoBRyTecnologia />
              </Link>
            </div>
            <button
              className={`Nav__mobile-button ${
                isMobileMenuOpen ? 'Nav__mobile-button--open' : ''
              }`}
              onClick={() => setisMobileMenuOpen(!isMobileMenuOpen)}
            >
              <div />
              <div />
              <div />
            </button>
          </div>
          <div className="Nav__menu-wrapper">
            <div className="Nav__desktop-menu">
              <Menu
                menuItems={menuItems}
                linkComponent={Link}
                onHandleMobile={value => handleMobileOpen(value)}
              />
              <div className="Nav__buttons-wrapper">
                <div className="Nav__button-wrapper">
                  <Button
                    linkComponent={Link}
                    variant="secondary"
                    href={`${domain}/contato`}
                  >
                    Contate Vendas
                  </Button>
                </div>
                {showTesteGratis && (<div className="Nav__button-wrapper">
                  <Button linkComponent={Link} href={`${domain}/teste-gratis`}>
                    Teste grátis
                  </Button>
                </div>)}
              </div>
            </div>
          </div>
        </div>
      </nav>
      {isMobileMenuOpen && (
        <div className="Nav__mobile-menu">
          <Menu
            menuItems={menuItems}
            isMobile={true}
            linkComponent={Link}
            onHandleMobile={value => handleMobileOpen(value)}
          />
          <div className="Nav__buttons-wrapper">
            <div className="Nav__button-wrapper">
              <Button
                linkComponent={Link}
                variant="secondary"
                href={`${domain}/contato`}
                onClick={() => setisMobileMenuOpen(false)}
              >
                Contate Vendas
              </Button>
            </div>
            {showTesteGratis && (<div className="Nav__button-wrapper">
              <Button
                linkComponent={Link}
                href={`${domain}/teste-gratis`}
                onClick={() => setisMobileMenuOpen(false)}
              >
                Teste grátis
              </Button>
            </div>)}
          </div>
        </div>
      )}
    </>
  )
}

Nav.propTypes = {
  domain: PropTypes.string,
  linkComponent: PropTypes.elementType,
  onMobileOpen: PropTypes.any,
  showTesteGratis: PropTypes.bool
}

export default Nav
