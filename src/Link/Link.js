import React from 'react'
import PropTypes from 'prop-types'
import './Link.scss'

const Link = ({ children, href, target = '', className }) => {
  return (
    <a className={`Link ${className || ''}`} href={href} target={target}>
      {children}
    </a>
  )
}

Link.propTypes = {
  children: PropTypes.node,
  href: PropTypes.string.isRequired,
  target: PropTypes.string,
  className: PropTypes.string
}

export default Link
